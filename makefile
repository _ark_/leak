
clean:
	rm callyouback.so callyouback
build: callyouback.c leak.lisp
	gcc -shared -o callyouback.so -fPIC callyouback.c
run: build
	LD_LIBRARY_PATH=./ sbcl leak.lisp
run-c: build
	gcc -pthread -o callyouback callyouback.c
	./callyouback
